#!/usr/bin/env python3

__author__ = 'Przemysław Kondaszewski'
__copyright__ = "Copyright (c) 2021 Przemysław Kondaszewski"
__email__ = "kondaszewskiprz@gmail.com"
__version__ = '1.0'

from arper import parse_args
import random
import sys
import os

HANGMANPICS = ['''
      |
      |
      |
      |
      |
=========''', '''
  +---+
      |
      |
      |
      |
      |
=========''', '''
  +---+
  |   |
      |
      |
      |
      |
=========''', '''
  +---+
  |   |
  O   |
      |
      |
      |
=========''', '''
  +---+
  |   |
  O   |
  |   |
      |
      |
=========''', '''
  +---+
  |   |
  O   |
 /|   |
      |
      |
=========''', '''
  +---+
  |   |
  O   |
 /|\  |
      |
      |
=========''', '''
  +---+
  |   |
  O   |
 /|\  |
 /    |
      |
=========''', '''
  +---+
  |   |
  O   |
 /|\  |
 / \  |
      |
=========''']

# animals, plants, names, computer / 3-5 letters
easy_words = ('krowa mucha pies kot osioł komar wąż kura kogut jeleń '
              'owies róża klon lipa mięta wrzos dąb żyto aloes sosna '
              'anna magda maria adam eryk piotr ela ewa jola tomek '
              'tab ctrl alt ekran mysz cd usb email pdf plik '.split())

# animals, plants, names, computer / 6-9 letters
medium_words = ('szympans papuga delfin bocian amstaf jaszczur wielbłąd chomik orangutan hipopotam '
                'paproć kaktus tulipan goździk jabłko papryka marchewka kukurydza ananas truskawka '
                'andrzej marian ewelina tadeusz monika hubert grażyna ryszard karolina dominika '
                'program aplikacja internet drukarka skaner monitor router zasilacz mikrofon pulpit ').split()

# adjectives - 10 letters
hard_words = ('bezsmakowy bezsmarowy bezstanowy bimetalowy biodrzasty '
              'dwusieczny fartuchowy kuriozalny humorzasty kasztelowy '
              'pieluchowy pogardliwy przesmutny spaleniowy urokliwszy '
              'alergiczny antyludzki bezpalcowy budowniczy cezaryjski '
              'edynburski elipsowaty freudowski hotelarski klarnetowy '
              'kastingowy klasztorny korzenisty letniskowy macierzowy '
              'malezyjski marcinkowy mieczowaty miechowaty obusieczny '
              'furgotliwy grudkowaty metaliczny nadgorliwy grzbietowy ').split()

words = [easy_words, medium_words, hard_words]
option_name = ["łatwy", "średni", "trudny"]
category_name = ["zwierzę", "roślina", "imie", "komputer", "przymiotnik"]


def game_setup(system_arguments) -> list:
    if (len(system_arguments) == 3) and (system_arguments[1]):
        if system_arguments[2] == option_name[0]:
            option = 0
        elif system_arguments[2] == option_name[1]:
            option = 1
        elif system_arguments[2] == option_name[2]:
            option = 2
        else:
            sys.exit(0)
    else:
        option = random.randint(0, 2)
        print("Poziom:", option_name[option])

    category = random.randint(0, 39)
    random_word = words[option][category]

    category_info = ""
    if option != 2:
        if 0 <= category < 10:
            category_info = category_name[0]
        elif 10 <= category < 20:
            category_info = category_name[1]
        elif 20 <= category < 30:
            category_info = category_name[2]
        elif 30 <= category < 40:
            category_info = category_name[3]
    else:
        category_info = category_name[4]

    answer = ""
    for _ in list(random_word):
        answer += '_ '

    lifes = 0
    used_letters = []

    return [random_word, category_info, answer, lifes, used_letters]


def output(category_info: str, answer: str) -> None:
    print()
    print(category_info)
    print(answer)


def game_logic(setup_values: list, letter: str) -> tuple:
    random_word = setup_values[0]
    answer = setup_values[2]
    lifes = setup_values[3]
    used_letters = setup_values[4]
    if len(letter) > 1:
        if letter == random_word:
            return 1, answer, lifes, used_letters
        else:
            return 2, answer, lifes, used_letters
    else:
        if letter.isalpha() and letter.islower() and not used_letters.__contains__(letter):
            if random_word.__contains__(letter):
                index = 0
                for char in random_word:
                    if char == letter:
                        answer = answer[:index] + char + answer[index + 1:]
                    index += 2
                if not answer.__contains__('_ '):
                    return 5, answer, lifes, used_letters
                return 3, answer, lifes, used_letters
            else:
                lifes += 1
                if lifes == 9:
                    return 4, answer, lifes, used_letters
            used_letters.append(letter)
            return 6, answer, lifes, used_letters
        else:
            return 7, answer, lifes, used_letters


if __name__ == "__main__":
    os.system("cls")
    parse_args(sys.argv[1:])
    game_values = game_setup(sys.argv)
    letter_char = False
    print(HANGMANPICS[0])
    while True:
        output(game_values[1], game_values[2])
        letter_char = input("Podaj litere lub haslo: ")
        update = game_logic(game_values, letter_char)
        game_values[2] = update[1]
        game_values[3] = update[2]
        os.system("cls")
        if update[0] == 1:
            print("Gratulacje! O to chodzilo.")
            break
        elif (update[0] == 2) or (update[0] == 4):
            print(HANGMANPICS[8])
            print("Pomylka... Gra przegrana. Poprawna odpowiedz to", game_values[0])
            break
        elif update[0] == 5:
            print()
            print(game_values[0])
            print("Odkryles wszystkie litery. Brawo!")
            break
        elif update[0] == 6:
            print(HANGMANPICS[update[2]-1])
            print("Wykorzystane litery: ", update[3])
            continue
        elif update[0] == 7:
            print("Nie podano poprawnego znaku (mala litera) lub zostala juz wypisana. Sprobuj ponownie")
        else:
            print(HANGMANPICS[update[2]])
            print("Wykorzystane litery: ", update[3])
    input("Naciśnij dowolny przycisk")
    os.system("cls")
